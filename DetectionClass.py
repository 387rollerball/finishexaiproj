
from matplotlib import pyplot as plt
from ImageAI.imageai.Detection import VideoObjectDetection
from ImageAI.imageai.Detection import ObjectDetection
import cv2
import time
import os
import queue
from motpy import Detection, MultiObjectTracker, Model
import matplotlib.patches as patches
from PIL import Image
import threading
from IPython import embed
from ObjectIdentificationAPI import sendIdentificationRequest


tracker = MultiObjectTracker(dt=0.1)
execution_path = os.getcwd()

color_index = {'bus': 'red', 'handbag': 'steelblue', 'giraffe': 'orange', 'spoon': 'gray', 'cup': 'yellow', 'chair': 'green', 'elephant': 'pink', 'truck': 'indigo', 'motorcycle': 'azure', 'refrigerator': 'gold', 'keyboard': 'violet', 'cow': 'magenta', 'mouse': 'crimson', 'sports ball': 'raspberry', 'horse': 'maroon', 'cat': 'orchid', 'boat': 'slateblue', 'hot dog': 'navy', 'apple': 'cobalt', 'parking meter': 'aliceblue', 'sandwich': 'skyblue', 'skis': 'deepskyblue', 'microwave': 'peacock', 'knife': 'cadetblue', 'baseball bat': 'cyan', 'oven': 'lightcyan', 'carrot': 'coldgrey', 'scissors': 'seagreen', 'sheep': 'deepgreen', 'toothbrush': 'cobaltgreen', 'fire hydrant': 'limegreen', 'remote': 'forestgreen', 'bicycle': 'olivedrab', 'toilet': 'ivory', 'tv': 'khaki', 'skateboard': 'palegoldenrod', 'train': 'cornsilk', 'zebra': 'wheat', 'tie': 'burlywood', 'orange': 'melon', 'bird': 'bisque',
               'dining table': 'chocolate', 'hair drier': 'sandybrown', 'cell phone': 'sienna', 'sink': 'coral', 'bench': 'salmon', 'bottle': 'brown', 'car': 'silver', 'bowl': 'maroon', 'tennis racket': 'palevilotered', 'airplane': 'lavenderblush', 'pizza': 'hotpink', 'umbrella': 'deeppink', 'bear': 'plum', 'fork': 'purple', 'laptop': 'indigo', 'vase': 'mediumpurple', 'baseball glove': 'slateblue', 'traffic light': 'mediumblue', 'bed': 'navy', 'broccoli': 'royalblue', 'backpack': 'slategray', 'snowboard': 'skyblue', 'kite': 'cadetblue', 'teddy bear': 'peacock', 'clock': 'lightcyan', 'wine glass': 'teal', 'frisbee': 'aquamarine', 'donut': 'mincream', 'suitcase': 'seagreen', 'dog': 'springgreen', 'banana': 'emeraldgreen', 'person': 'honeydew', 'surfboard': 'palegreen', 'cake': 'sapgreen', 'book': 'lawngreen', 'potted plant': 'greenyellow', 'toaster': 'ivory', 'stop sign': 'beige', 'couch': 'khaki'}

resized = False
oldtracks = []
FrameQueue = queue.Queue()
running = False
StartTime = time.time()

# the main function that is gonna handle all in queue all of the detected frames


class DetectionClass(object):
    def __init__(self):
        lock = threading.Condition()
        threading._start_new_thread(
            self.__load, (tuple([lock])))

        with lock:
            lock.wait()
        print("finished the loading")

    def __load(self, lock):
        self.videoQueue = queue.Queue()
        self.detector = VideoObjectDetection()

        # setting the model we want to use to do the object detection
        self.detector.setModelTypeAsYOLOv3()
        self.detector.setModelPath(os.path.join(
            execution_path, "yolo.h5"))

        # this is an intresting part, you can change this parameter to
        # gain speed but reduce accuracy which is very not ideal in this situation
        self.detector.loadModel(detection_speed="fast")

        # debugging with a screen
        # plt.show()

        # this is used to determain which objects we want to scan for
        self.customObjDetector = self.detector.CustomObjects(
            person=True, car=True)
        with lock:
            lock.notify()
        self.__videoQueueListener()

    def __videoQueueListener(self):
        while True:
            currVid, VId = self.videoQueue.get(block=True)
            a = time.time()
            cam = cv2.VideoCapture(os.path.join(execution_path, currVid))
            video_path = self.detector.detectCustomObjectsFromVideo(custom_objects=self.customObjDetector, camera_input=cam, frame_detection_interval=3,
                                                                    save_detected_video=False,  display_percentage_probability=False,
                                                                    display_object_name=False, frames_per_second=30, per_frame_function=self.__PutFrameInHandleQueue,
                                                                    return_detected_frame=True, thread_safe=True, minimum_percentage_probability=90, extra_args=VId)
            print("it took about{}".format(time.time() - a))

    def __FrameHandleInvoker(self):
        while True:
            a, b, c, d, e = FrameQueue.get(block=True)
            self.__HandleFrame(a, b, c, d, e)

    def __PutFrameInHandleQueue(self, frame_number, output_array, output_count, returned_frame, VId):
        global running
        FrameQueue.put((frame_number, output_array,
                        output_count, returned_frame, VId))
        if not running:
            running = True
            threading._start_new_thread(
                self.__FrameHandleInvoker, ())

        """
        this function is ment to add new videos into the queue to analyze

        @input_file_path = the path to the file to analyze
        @Vid = is the ID of the file, so that we can update it correctly in the DB
        """

    def AddNewVideo(self, input_file_path, Vid):
        self.videoQueue.put((input_file_path, Vid))

    def __HandleFrame(self, frame_number, output_array, output_count, returned_frame, VId):
        CordsList = []
        for detect in output_array:
            CordsList.append(
                Detection(box=detect["box_points"], score=detect["percentage_probability"]))

        tracker.step(detections=CordsList)
        tracks = tracker.active_tracks()

        global resized
        global oldtracks

        TempTracking = []

        for track in tracks:
            TempTracking.append(track.id)
            if track.id not in oldtracks:
                img = Image.fromarray(returned_frame)
                c_img = img.crop(track.box)
                height, width = c_img.size
                if (height > 50 and width > 50):
                    objtype = "Unknown"
                    for pos in output_array:
                        if track.box.tolist() == pos["box_points"]:
                            objtype = pos["name"]
                    c_img.save(execution_path + "/Outputs/" +
                               VId + "-" + objtype + "-" + track.id[:5] + ".jpg")
                    sendIdentificationRequest(objtype, c_img, VId)

                # embed()
                print("found a new one")

        oldtracks = TempTracking

    def __HandleFrameDebug(self, frame_number, output_array, output_count, returned_frame, VId):
        # print("handling a frame")
        # global a
        # print("fps:{}\r".format(str(time.time()-a/frame_number)))
        CordsList = []
        for detect in output_array:
            CordsList.append(
                Detection(box=detect["box_points"], score=detect["percentage_probability"]))

        tracker.step(detections=CordsList)
        tracks = tracker.active_tracks()

        # debugging with a screen
        plt.clf()

        global resized
        global oldtracks

        # debgging with a screen

        if (resized == False):
            manager = plt.get_current_fig_manager()
            manager.resize(width=2000, height=1000)
            resized = True

        plt.subplot(1, 2, 1)
        plt.title("Frame : " + str(frame_number))
        plt.axis("off")
        plt.imshow(returned_frame, interpolation="none")

        # TempTracking = []

        for track in tracks:
            # debgging with a screen
            rect = patches.Rectangle(track.box,
                                     70,
                                     100,
                                     linewidth=2,
                                     edgecolor='cyan',
                                     fill=False)
            plt.text(int(track.box[0]), int(track.box[1]), track.id[:4])
            TempTracking.append(track.id)
            if track.id not in oldtracks:
                img = Image.fromarray(returned_frame)
                c_img = img.crop(track.box)
                height, width = c_img.size
                if (height > 50 and width > 50):
                    c_img.save(execution_path+"/Outputs/" +
                               VId + "-" + track.id[:5]+".jpg")
                # embed()
                print("found a new one")

        # debgging with a screen
        # in case of debugging turn these lines on
        try:
            ax = plt.gca()
            ax.add_patch(rect)
        except:
            pass

        oldtracks = TempTracking

        # debgging with a screen
        plt.show()
        plt.pause(0.0001)


if __name__ == "__main__":
    # plt.show()
    dec = DetectionClass()
    dec.AddNewVideo("video1.h264", "video1")
    dec.AddNewVideo("video2.h264", "video2")
    dec.AddNewVideo("video3.h264", "video3")

    while(True):
        time.sleep(500)
        pass
    # # this section is just initializing some things for the detection to work as needed
    # videofiles = ["traffic.mp4"]
    # execution_path = os.getcwd()
    # detector = VideoObjectDetection()
    # # setting the model we want to use to do the object detection
    # detector.setModelTypeAsYOLOv3()
    # detector.setModelPath(os.path.join(
    #     execution_path, "yolo.h5"))

    # # this is an intresting part, you can change this parameter to
    # # gain speed but reduce accuracy which is very not ideal in this situation
    # detector.loadModel(detection_speed="fast")

    # # debugging with a screen

    # # this is used to determain which objects we want to scan for
    # custom = detector.CustomObjects(person=True, car=True)

    # # this could become a queue of videofiles to make it async the thread safe
    # for Vfile in videofiles:
    #     cam = cv2.VideoCapture(os.path.join(execution_path, Vfile))
    #     StartTime = time.time()
    #     # plt.show()
    #     # video_path = detector.detectCustomObjectsFromVideo(custom_objects=custom, input_file_path=os.path.join(execution_path, "traffic_Trim.mp4"), frame_detection_interval=2,
    #     #  display_percentage_probability=False,display_object_name=False,output_file_path=os.path.join(execution_path, "Traffic_trim_detected"), frames_per_second=30, log_progress=True,per_frame_function=forFrame,return_detected_frame=True)
    #     video_path = detector.detectCustomObjectsFromVideo(custom_objects=custom, camera_input=cam, frame_detection_interval=3,
    #                                                        save_detected_video=False,  display_percentage_probability=False, display_object_name=False, frames_per_second=30, per_frame_function=HandleFrame, return_detected_frame=True)

    #     print("3frame async the scan took about " +
    #           str((time.time() - StartTime)))
