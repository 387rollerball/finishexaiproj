FROM nvidia/cuda:10.0-cudnn7-devel-ubuntu18.04
COPY . /app/
RUN apt-get update -y && apt-get install -y python3.6 && apt install -y python3-pip && pip3 install --upgrade pip && pip3 install -r /app/requirements.txt
RUN apt-get update && \
        apt-get install -y libsm6 libxext6 libxrender-dev
ENTRYPOINT ["python3.6", "/app/server.py"]
EXPOSE 5000
