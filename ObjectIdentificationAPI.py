import requests
import json


def sendIdentificationRequest(objtype, picture, Vid):
    r = requests.post("http://127.0.0.1:5000/" +
                      objtype, files=picture, data=(Vid))


jsonTemplate = """
{
    "vod_id":"temp",
    "entity_type":"temp",
    "description":"temp"
}"""


def sendDbUpdate(objtype, Vid, Info):
    jsonstr = json.loads(jsonTemplate)
    jsonstr["vod_id"] = Vid
    if objtype == "person":
        jsonstr["entity_type"] = 0
    else:
        jsonstr["entity_type"] = 1
    jsonstr["description"] = Info
    r = requests.post("http://127.0.0.1:5000/" +
                      "SomeApi", json=json.dumps(jsonstr))
