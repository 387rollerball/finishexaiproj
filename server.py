import os
from flask import Flask, render_template, request, Response
from werkzeug.utils import secure_filename
from DetectionClass import DetectionClass
from flask_json import FlaskJSON, JsonError, json_response, as_json
from ObjectIdentificationAPI import *

app = Flask(__name__, static_url_path="")
FlaskJSON(app)

detector = DetectionClass()


@app.route('/')
def upload_file():
    return render_template('upload.html')


@app.route('/identificationResult', methods=['GET', 'POST'])
def get_identification():
    if request.method == 'POST':
        data = request.get_json(force=True)
        try:
            Vid = str(data['Vid'])
            ET = int(data['EntityType'])
            entityInfo = str(data["entityInfo"])
            sendDbUpdate(ET, Vid, entityInfo)
            return Response("all good in da hood")
        except (KeyError, TypeError, ValueError):
            raise JsonError(description='Invalid value.')
            return json_response("not all good in da hood")


@app.route('/uploader', methods=['GET', 'POST'])
def upload_filee():
    global detector
    if request.method == 'POST':
        f = request.files['file']
        f.save(secure_filename(f.filename))
        detector.AddNewVideo(secure_filename(f.filename),
                             secure_filename(f.filename))
        return 'file uploaded successfully'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=False)
